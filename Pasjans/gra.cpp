#include "gra.h"
#include <iostream>
#include <string>
using namespace std;

struct XY
{
	int x;
	int y;
};

XY tab[5][8] = {
	{ { 7, 4 }, { 18, 4 }, { 29, 4 }, { 40, 4 }, { 51, 4 }, { 62, 4 }, { 73, 4 }, { 84, 4 } },
	{ { 7, 7 }, { 18, 7 }, { 29, 7 }, { 40, 7 }, { 51, 7 }, { 62, 7 }, { 73, 7 }, { 84, 7 } },
	{ { 7, 10 }, { 18, 10 }, { 29, 10 }, { 40, 10 }, { 51, 10 }, { 62, 10 }, { 73, 10 }, { 84, 10 } },
	{ { 7, 13 }, { 18, 13 }, { 29, 13 }, { 40, 13 }, { 51, 13 }, { 62, 13 }, { 73, 13 }, { 84, 13 } },
	{ { 7, 16 }, { 18, 16 }, { 29, 16 }, { 40, 16 }, { 51, 16 }, { 62, 16 }, { 73, 16 }, { 84, 16 } }
};


Gra::Gra()
{
	Talia Rozdanie;
	Wyglad Plansza;
	Sterowanie Odczyt;
	//Gra dzia�a do momenu nacisniecia klawisza ESC//
	int licznik = 1;
	while (potwierdzenie==false)
	{
		//cout << tab[0][2].x << tab[0][2].y;
		//Klasa Talia//
		Rozdanie.wydane_karty;	//odwo�anie do tablicy z kartami na planszy;
		Rozdanie.pozosta�e_karty;	//tablica ukrytych kart 
		Rozdanie.SzukajKarty(Rozdanie.Head, 1);	//funkcja szuka karty o konkretny ID i przypisuje j� do wska�nika Current
		Rozdanie.Current->wartosc_karty;	//Pobranie dowolnej warto�ci z szukanej karty
		//Klasa Sterowanie//
		Odczyt.zczytaj();	//funkcja zczytuje znak z klawiatury
		Odczyt.pozycja; //numer kolumny w tablicy z kartami na planszy 0-8
		Odczyt.zakryta;	//odwo�anie czy zosta�a wybrana karta zakryta false/true
		Odczyt.zaznaczona;	//funkcja potwierdzaj�ca wybor karty false/true
		Odczyt.strza�ki;
		//Klasa wyglad//
		//...//

		if (Nowa_gra == false )
		{

			// poruszanie sie po ekranie startowym
			if (Odczyt.strza�ki == 'u')
			{
				if (pozycja_wyboru == 0)
					pozycja_wyboru = 2;
				else if (pozycja_wyboru == 1)
					pozycja_wyboru--;
				else if (pozycja_wyboru == 2)
					pozycja_wyboru--;
				Plansza.EkranStartowyWyboru(pozycja_wyboru);
				Odczyt.strza�ki = ' ';
			}
			else if (Odczyt.strza�ki == 'd')
			{
				if (pozycja_wyboru == 2)
					pozycja_wyboru = 0;
				else if (pozycja_wyboru == 1)
					pozycja_wyboru++;
				else if (pozycja_wyboru == 0)
					pozycja_wyboru++;
				Plansza.EkranStartowyWyboru(pozycja_wyboru);
				Odczyt.strza�ki = ' ';
			}
			//Koniec programu
			if (Odczyt.zaznaczona == true && pozycja_wyboru == 2)
				potwierdzenie = true;
			//Nowa gra
			if (Odczyt.zaznaczona == true && pozycja_wyboru == 0)
			{
				wyzerowanie();
				Nowa_gra = true;
				Odczyt.zaznaczona = false;
				Plansza.WyswietlaniePlanszy();
				int x = 0, y = 0;
				for (int i = 0; i < 5; i++)
				{
					for (int j = 0; j < 8; j++)
					{
						Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.wydane_karty[i][j]);
						//Rozdanie.Current;
						Plansza.WypelnianieKarty(x, y, Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
						x += 11;
					}
					x = 0;
					y += 3;
				}
				Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.pozosta�e_karty[0]);
				Plansza.WydajZPozosta�ych(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
				uzyte_karty[zebrane_karty] = Rozdanie.pozosta�e_karty[0];
				zebrane_karty++;
				
			}
		}
		// Poruszanie si� w grze
		if (Nowa_gra == true)
		{
			
			Plansza.Wynik(punkty);
			if (Odczyt.pozycja != 8 && Rozdanie.wydane_karty[0][Odczyt.pozycja] == 0 && Odczyt.strza�ki == 'r')
			{
				do
				{
					if (Odczyt.pozycja == 7)
						Odczyt.pozycja = 0;
					Odczyt.pozycja++;
					
				} while (Rozdanie.wydane_karty[0][Odczyt.pozycja] == 0);
			}
			else if (Odczyt.pozycja != 7 && Rozdanie.wydane_karty[0][Odczyt.pozycja] == 0 && Odczyt.strza�ki=='l' )
			{
				do
				{
					if (Odczyt.pozycja == 0)
						Odczyt.pozycja = 8;
					Odczyt.pozycja--;
					
				} while (Rozdanie.wydane_karty[0][Odczyt.pozycja] == 0);
			}
		

			int i;
			if (Rozdanie.wydane_karty[0][Odczyt.pozycja] != 0 && Odczyt.zakryta == false)
			{
				i = 4;
				while (Rozdanie.wydane_karty[i][Odczyt.pozycja] == 0)
				{
					i--;
				};

				Plansza.OdznaczKarte(tab[0][0].x, tab[0][0].y);
				Plansza.ZaznaczKarte(tab[i][Odczyt.pozycja].x, tab[i][Odczyt.pozycja].y);

				if (Odczyt.zaznaczona == true)
				{
					// glowana mechanika 
					
					Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.wydane_karty[i][Odczyt.pozycja]);
					
					Rozdanie.SzukajKartyDodatkowej(Rozdanie.Head, uzyte_karty[zebrane_karty-1]);
					

					if (Rozdanie.Current->wartosc_karty-1 == Rozdanie.tmp->wartosc_karty || Rozdanie.Current->wartosc_karty+1 == Rozdanie.tmp->wartosc_karty)

					{
						uzyte_karty[zebrane_karty] = Rozdanie.wydane_karty[i][Odczyt.pozycja];
						zebrane_karty++;
						kontroler++;
						punkty += 500;
						Plansza.Wynik(punkty);
						Plansza.UsuwanieKarty(tab[i][Odczyt.pozycja].x, tab[i][Odczyt.pozycja].y);
						Plansza.PrzeniesKarte(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
						Rozdanie.wydane_karty[i][Odczyt.pozycja] = 0;
						Plansza.OdznaczKarte(tab[0][0].x, tab[0][0].y);
						if (kontroler == 40)
						{
							Plansza.KoniecGry();
							break;
						}
						
						if (i > 0)
						{
							Plansza.ZaznaczKarte(tab[i - 1][Odczyt.pozycja].x, tab[i - 1][Odczyt.pozycja].y);
						}
						else
						{

							if (Odczyt.pozycja != 8)
							{

								do
								{
									if (Odczyt.pozycja == 7)
										Odczyt.pozycja = -1;
									Odczyt.pozycja++;
								} while (Rozdanie.wydane_karty[0][Odczyt.pozycja] == 0);
								i = 4;
								while (Rozdanie.wydane_karty[i][Odczyt.pozycja] == 0)
								{
									i--;
								};
								Plansza.ZaznaczKarte(tab[i][Odczyt.pozycja].x, tab[i][Odczyt.pozycja].y);
							}

						}

						//koniec mechaniki
					}
					
					else continue;
				
				}
				
			}
			pozycja_wyboru = Odczyt.pozycja;
			}
			
			if (Odczyt.zakryta == true)
			{
				int i = 1;
				
				Plansza.OdznaczKarte(tab[0][0].x, tab[0][0].y);
				Plansza.ZaznaczKarte(40, 21);

				if (Odczyt.zaznaczona == true)
				{
					if (licznik < 12)
					{
						uzyte_karty[zebrane_karty] = Rozdanie.pozosta�e_karty[licznik];
						zebrane_karty++;

						Rozdanie.SzukajKarty(Rozdanie.Head, Rozdanie.pozosta�e_karty[licznik]);
						licznik++;
						Plansza.WydajZPozosta�ych(Rozdanie.Current->wartosc_karty, Rozdanie.Current->znak_karty, Rozdanie.Current->kolor_karty);
						if (licznik == 12)
						{
							COORD c;
							c.X = 41;
							c.Y = 22;
							SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
							cout << "#######";
						}
					}
				}

			}
			//cout << uzyte_karty[zebrane_karty-1];
			if (Odczyt.koniec == true)
			{
				Odczyt.koniec = false;
				Odczyt.zaznaczona = false;
				Plansza.Komunikat();
				Odczyt.zczytaj();
				
				if (Odczyt.zaznaczona == true)
					break;
				else
					Plansza.UsunKomunikat();
				Odczyt.koniec = false;
				Odczyt.zaznaczona = false;
				
			}
		}
		
	
}
void Gra::wyzerowanie()
{
	for (int i = 0; i < 52; i++)
	{
		//uzyte_karty[i] = NULL;
	}
}
Gra::~Gra()
{
}
